// tabs
document.querySelector('.tabs').addEventListener("click", trackTabs);
let tabBody = document.querySelectorAll('.show-tab');

for (let i = 1; i < tabBody.length; i++) {
    tabBody[i].style.display = 'none';
}

function trackTabs(event) {
    if (event.target.className == 'tabs-title') {
        let tabCaption = document.getElementsByClassName('tabs-title');
        let getDataTab = event.target.getAttribute('data-tab');

        for (let i = 0; i < tabCaption.length; i++) {
            tabCaption[i].classList.remove('active');
        }
        event.target.classList.add('active');

        for (let i = 0; i < tabBody.length; i++) {
            if (getDataTab == i) {
                tabBody[i].style.display = 'block';
            } else {
                tabBody[i].style.display = 'none';
            }
        }
    }
}

// END tabs

// Initializing slider
$(document).ready(function () {
    $('.slider-testimonials').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: false,
        asNavFor: '.slider-nav'
    });

    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-testimonials',
        dots: false,
        // centerMode: true,
        arrows: true,
        focusOnSelect: true
    });
});
// END Initializing slider

// Our work section

$('.our-work_menu-item').click(function () {
    const selectedCategory = $(this).data('category');

    $('.our-work_menu-item-active').removeClass('our-work_menu-item-active');
    $(this).addClass('our-work_menu-item-active');

    $('.category').each(function (index, item) {
        if ($(item).data('category') === selectedCategory || selectedCategory === undefined) {
            $(item).show();
        } else {
            $(item).hide();
        }
    });
});

// load more work
$('.our-work-btn').click(function () {
    $(this).parent().find(".loader-icon").show().delay(2000).queue(function (n) {
        $('.img-load-category').show('500');
        $('.loader-icon').hide();
        $('.our-work-btn').hide();
    });
});

// add masonry for Gallery of best images
let masonryOptions = {
    // options
    itemSelector: '.grid-item',
    columnWidth: '.grid-sizer',
    percentPosition: true
};

let $grid = $('.grid').masonry(masonryOptions);

// show imgs galery
$('.best-images-button').click(function () {
    $(this).parent().find('.loader-icon').show().delay(2000).queue(function (n) {
        $('.grid-item').removeClass('hidden-img');
        $('.grid').css('height', 'auto');
        $grid.masonry(masonryOptions);
        $('.loader-icon').hide();
        $('.best-images-button').hide();
    });
});


